
//Custom helpers

//
//
//
//
// // // //

function getKeyByValue( array,  value ) {
    for( var prop in array ) {
        if( array.hasOwnProperty( prop ) ) {
             if( array[ prop ] === value )
                 return prop;
        }
    }
}

//CSVString — a class for working with CSV Strings.
//Including toJSON method, which converts CSV table into JSON object of Object[X][Y] format,
//where X and Y are first column and first row values of CSV table

function CSVString ( string ) {
    
    this.string = string;
    
    this.toJSON = function () {  
        var rows = this.string.split("\\n");
        var xArray = [];
        var yArray = [];
        var JSONObject = {};
        $.each(rows, function (rowNumber, rowString) {
            if (rowNumber < 1) {
                $.each(rowString.split(";"), function (column, value) {
                    if(column > 0) {
                       yArray.push(value);
                    }
                });
            }else {
                xArray.push(rowString.split(";")[0]);
            }
        });
        var i = 1;
        $.each(xArray, function (row, X) {
            JSONObject[X] = {};
            var j = 1;
            $.each(yArray, function (column, Y) {
                JSONObject[X][Y] = rows[i].split(";")[j];
                j++;
            });
            i++;
        });
        return JSONObject;
    }
};

// // // // 
//
//
//
//

//Mockup matrices as objects

//
//
//
//
// // // //

var $delta = { 
            "0.1" : {
                "1.1" : "1",
                "1.2" : "0",
                "1.3" : "0"
            },
            "0.2" : {
                "1.1" : "0",
                "1.2" : "1",
                "1.3" : "0"
            },
            "0.3" : {
                "1.1" : "0",
                "1.2" : "0",
                "1.3" : "1"
            }
        };

var $beta = {
            "0.66" : {
                "1.16" : "3",
                "1.26" : "4"
            },
            "0.26" : {
                "1.16" : "5",
                "1.26" : "6"
            }
        };

//var $matrix = new CSVString(";1.1;1.2;1.3\\n0.1;1;0;0\\n0.2;0;1;0\\n0.3;0;0;1");

//var $Fmatrix = new CSVString(";0.1;0.2;0.3;0.4;0.5;0.6\\n1.1;1;0;0;0;0;0\\n1.2;0;1;0;0;0;0\\n1.3;0;0;1;0;0;0\\n1.4;0;0;0;1;0;0\\n1.5;0;0;0;0;1;0\\n1.6;0;0;0;0;0;1");
var $Fmatrix = new CSVString(";1;0.95;0.90;0.85;0.8;0.75;0.7;0.65;0.6;0.55;0.5;0.45;0.4;0.35;0.3;0.25;0.2;0.15;0.1;0.05;0\\n1.00;0.84833;0.87727;0.89552;0.91020;0.92264;0.93345;0.94298;0.95144;0.95900;0.96576;0.97180;0.97718;0.98194;0.98612;0.98974;0.99282;0.99535;0.99735;0.99880;0.99969;1\\n0.95;0.86783;0.88982;0.90561;0.91859;0.92971;0.93944;0.94805;0.95573;0.96261;0.96877;0.97428;0.97919;0.98354;0.98736;0.99066;0.99346;0.99577;0.99759;0.99891;0.99972;1\\n0.90;0.88418;0.90175;0.91531;0.92666;0.93655;0.94524;0.95298;0.95990;0.96612;0.97169;0.97668;0.98114;0.98509;0.98855;0.99155;0.99409;0.99618;0.99783;0.99902;0.99975;1\\n0.85;0.89870;0.91296;0.92456;0.93444;0.94314;0.95085;0.95774;0.96393;0.96951;0.97452;0.97901;0.98302;0.98658;0.98970;0.99240;0.99469;0.99657;0.99805;0.99912;0.99978;1\\n0.80;0.91176;0.92344;0.93329;0.94185;0.94944;0.95622;0.96231;0.96781;0.97276;0.97723;0.98124;0.98483;0.98801;0.99080;0.99322;0.99526;0.99695;0.99827;0.99922;0.99980;1\\n0.75;0.92356;0.93318;0.94150;0.94885;0.95542;0.96132;0.96668;0.97151;0.97588;0.97983;0.98338;0.98656;0.98938;0.99185;0.99399;0.99581;0.99730;0.99847;0.99931;0.99983;1\\n0.70;0.93426;0.94217;0.94917;0.95543;0.96107;0.96618;0.97082;0.97503;0.97884;0.98230;0.98541;0.98820;0.99068;0.99285;0.99473;0.99633;0.99764;0.99866;0.99940;0.99985;1\\n0.65;0.94394;0.95045;0.95629;0.96157;0.96637;0.97074;0.97472;0.97835;0.98164;0.98464;0.98732;0.98975;0.99190;0.99380;0.99543;0.99682;0.99795;0.99884;0.99948;0.99987;1\\n0.60;0.95270;0.95803;0.96286;0.96727;0.97130;0.97499;0.97837;0.98146;0.98427;0.98683;0.98913;0.99121;0.99306;0.99467;0.99608;0.99727;0.99825;0.99901;0.99956;0.99989;1\\n0.55;0.96060;0.96492;0.96888;0.97252;0.97586;0.97894;0.98176;0.98435;0.98672;0.98887;0.99082;0.99257;0.99413;0.99550;0.99669;0.99770;0.99852;0.99916;0.99963;0.99990;1\\n0.50;0.96769;0.97115;0.97434;0.97730;0.98003;0.98256;0.98488;0.98702;0.98897;0.99076;0.99237;0.99383;0.99512;0.99626;0.99725;0.99809;0.99877;0.99931;0.99969;0.99992;1\\n0.45;0.97400;0.97673;0.97927;0.98163;0.98382;0.98584;0.98772;0.98945;0.99103;0.99248;0.99379;0.99498;0.99603;0.99696;0.99776;0.99844;0.99900;0.99944;0.99975;0.99994;1\\n0.40;0.97958;0.98169;0.98366;0.98550;0.98721;0.98880;0.99028;0.99164;0.99289;0.99404;0.99508;0.99601;0.99685;0.99759;0.99823;0.99877;0.99921;0.99955;0.99980;0.99995;1\\n0.35;0.98444;0.98603;0.98751;0.98890;0.99020;0.99142;0.99254;0.99358;0.99454;0.99542;0.99622;0.99694;0.99758;0.99815;0.99864;0.99905;0.99939;0.99966;0.99985;0.99996;1\\n0.30;0.98862;0.98976;0.99084;0.99186;0.99280;0.99369;0.99451;0.99527;0.99598;0.99662;0.99721;0.99774;0.99822;0.99864;0.99900;0.99930;0.99955;0.99975;0.99989;0.99997;1\\n0.25;0.99212;0.99291;0.99365;0.99435;0.99500;0.99561;0.99618;0.99671;0.99720;0.99765;0.99806;0.99843;0.99876;0.99905;0.99930;0.99952;0.99969;0.99982;0.99992;0.99998;1\\n0.20;0.99498;0.99547;0.99594;0.99638;0.99680;0.99719;0.99755;0.99789;0.99821;0.99849;0.99879;0.99899;0.99920;0.99939;0.99955;0.99969;0.99980;0.99989;0.99995;0.99999;1\\n0.15;0.99718;0.99746;0.99772;0.99797;0.99820;0.99842;0.99862;0.99881;0.99899;0.99915;0.99930;0.99943;0.99955;0.99966;0.99975;0.99982;0.99989;0.99994;0.99997;0.99999;1\\n0.10;0.99875;0.99887;0.99899;0.99910;0.99920;0.99930;0.99939;0.99947;0.99955;0.99962;0.99969;0.99975;0.99980;0.99985;0.99989;0.99992;0.99995;0.99997;0.99999;1;1\\n0.05;0.99969;0.99972;0.99975;0.99977;0.99980;0.99982;0.99985;0.99987;0.99989;0.99991;0.99992;0.99994;0.99995;0.99996;0.99997;0.99998;0.99999;0.99999;1;1;1\\n0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;0");
var $FmatrixJSON = $Fmatrix.toJSON();

// // // // 
//
//
//
//


//Calculation methods

//
//
//
//
// // // //


var wWProduct = function ( variables ) {

    var w = variables.w; 
    var W = variables.W;

    return w * W;
}

var twoCoils = function ( variables ) {
    var pi = Calc.config.$constants.pi;
    var mu = Calc.config.$constants.mu;


    var w = variables.w; 
    var W = variables.W;
    var a = variables.a;
    var A = variables.A;
    var d = variables.d;
    var D = variables.D;
    var F = variables.matrix;

    var x4 = variables.x4;
    var x1 = x4 + A + a;
    var x2 = x4 + a;
    var x3 = x4 + A;

    var l1 = Math.sqrt(Math.pow( D/2 , 2) + x1*x1);
    var l2 = Math.sqrt(Math.pow( D/2 , 2) + x2*x2);
    var l3 = Math.sqrt(Math.pow( D/2 , 2) + x3*x3);
    var l4 = Math.sqrt(Math.pow( D/2 , 2) + x4*x4);

    var delta = Calc.findClosest(F, d/D);

    var lamda1 = Calc.findClosest(F, Math.pow( D/(2*l1) ,2), "Y");
    var lamda2 = Calc.findClosest(F, Math.pow( D/(2*l2) ,2), "Y");
    var lamda3 = Calc.findClosest(F, Math.pow( D/(2*l3) ,2), "Y");
    var lamda4 = Calc.findClosest(F, Math.pow( D/(2*l4) ,2), "Y");

    var F1 = F[delta][lamda1];
    var F2 = F[delta][lamda2];
    var F3 = F[delta][lamda3];
    var F4 = F[delta][lamda4];

    var m1 = l1 * F1;
    var m2 = l2 * F2;
    var m3 = l3 * F3;
    var m4 = l4 * F4;

    return (pi/8) * mu * w * W * ( (d*d) / (a*A) ) * (l1 * F1 - l2 * F2  - l3 * F3  + l4 * F4);
}

// // // // 
//
//
//
//

// Calc Object as it goes:

//
//
//
//
// // // //

var Calc = {
 
    init: function( settings ) {

        Calc.$latexBaseURL = 'http://latex.codecogs.com/gif.latex?%5Cdpi%7B150%7D%20';
        
        Calc.config = {
            $container : $( "#landfill" ),
            $formId : 'mainForm',
            $latexEquation : '\\\\M_{12}=\\frac{\\pi}{8} \\mu_0 wW \\frac{d^2}{aA}(l_1F_1-l_2F_2-l_3F_3+l_4F_4) \\\\ \\\\ \
            l_i = \\sqrt{(D/2)^2 + {x_i}^2}, i = 1,2,3,4 \\\\ \\\\ \
            x_1 = x + \\frac{A+a}{2} \\\\ \\\\ \
            x_2 = x - \\frac{A-a}{2} \\\\ \\\\ \
            x_3 = x + \\frac{A-a}{2} \\\\ \\\\ \
            x_4 = x - \\frac{A+a}{2} \\\\ \\\\ \
            ',//'F = \\gamma (A,B) \\beta(C,D) ma',
            
            $constants : {
                pi: 3.14159265,
                mu: 1.25663706 * 0.000001
            },

            $dimScale: 0.001 ,

            $matrices: { 
                F: $FmatrixJSON ,

            },

            $inputs : { 
                w: { 
                    type: 'text',
                    id: 'w',
                    placeholder: 'Число витков на малой катушке',
                    label: 'w',
                } , 
                W: { 
                    type: 'text',
                    id: 'W',
                    placeholder: 'Число витков на большой катушке',
                    label: 'W'
                } , 
                a: { 
                    type: 'text',
                    id: 'a',
                    placeholder: 'Длина малой катушки, мм',
                    label: 'a',
                    format: 'float'
                } , 
                A: { 
                    type: 'text',
                    id: 'A',
                    placeholder: 'Длина большой катушки, мм',
                    label: 'A',
                    biggerThan: 'a',
                    format: 'float'
                } , 
                d: { 
                    type: 'text',
                    id: 'd',
                    placeholder: 'Диаметр малой катушки, мм',
                    label: 'd',
                    format: 'float'
                } , 
                D: { 
                    type: 'text',
                    id: 'D',
                    placeholder: 'Диаметр большой катушки, мм',
                    label: 'D',
                    biggerThan: 'd',
                    format: 'float'
                } , 
                x4: { 
                    type: 'text',
                    id: 'x4',
                    placeholder: 'Расстояние междуу гранями катуек, мм',
                    label: 'x_4',
                    format: 'float'
                } ,
                submit: {
                    type: 'submit',
                    id: 'submit',
                }
            },

            $calculateMethod: twoCoils
        };
        //var d = 7;
        //var D = 8;
       // var res = Calc.findClosest($JSONmatrix, Math.pow( D/(2*d) , 2), "Y");
       // alert(res + " : "  + Math.pow( D/(2*d) , 2));
        // allow overriding the default config
        $.extend( Calc.config, settings );
       
        Calc.setup();
    },
 
    setup: function() {
        Calc.config.$container.each( Calc.buildForm );
        Calc.config.$container.each( Calc.buildEquation );
    },

    buildEquation: function () {
        
        $latexURI = encodeURIComponent(Calc.config.$latexEquation);

        $('<img src = "' + Calc.$latexBaseURL + $latexURI + '">')
        .appendTo(Calc.config.$container)
        .wrap('<div class = "well col-md-8 text-center"></div>')
        ;
    },
    
    buildForm: function () {

        $('<form></form>')
            .appendTo($(this))
            .attr('id', Calc.config.$formId)
            .attr('role', 'form')
            .attr('class', 'form-horizontal col-md-4')
        .each( Calc.buildInputs );

        $form = $('#' + Calc.config.$formId);

        $('<div id = "result"></div>').appendTo($this);

        $form.submit( function ( event ) {   
            event.preventDefault();
            });
        //
        //trigger submit on blur??
        //animation onblur
        
        /*$form.find('input').blur( function () {
            var w = $('#w').val();
            var dx = 5;
            var x = 48.359;
            var width = $('#smallCoil').attr('width');
            var dx =  width / w;
            $('.smallLoop').remove();
            for (i = 1 ; i<=w ; i++) {
                $('<rect class = "smallLoop loop" x="' + x +'" y="91.815" fill="#A65A26" stroke="#000000" stroke-miterlimit="10" width="3.333" height="136.319"/>').appendTo('#Layer_1');
                x = x + dx;
            }


            $('#picture').html(function(){ return this.innerHTML });
                //$form.submit();
            });

        */

        $.validate({
                
                modules : 'security',

                validateOnBlur : false,
                
                onError : function() {
                 // alert('Validation failed');
                },
                onValidate : function() {
                    var callbackArr = []
                    $.each(Calc.config.$inputs , function ( input, params ) {
                        if ("biggerThan" in Calc.config.$inputs[input]) {
                            if (
                                $('#' + Calc.config.$inputs[input].biggerThan).val() > 
                                $('#' + Calc.config.$inputs[input].id).val()
                                ) {
                                callbackArr.push({
                                    element: $('#' + Calc.config.$inputs[input].id),
                                    message: Calc.config.$inputs[input].id + " должно быть не меньше " + Calc.config.$inputs[input].biggerThan
                                });
                            }
                        }
                    });
                      return callbackArr;
                },

                onSuccess : function() {
                    Calc.buildResult('#result');
                   // $form[0].reset();
                },

                language : { badInt: 'Только числа с плавающей точкой'}
              }); 
    },

    /*submitForm: function () {
        $(this).submit( function ( event ) {   
            $('#result').html('<div>' + ($('#mass').val() * $('#acc').val()) + '</div>');
            $(this)[0].reset();
            event.preventDefault();
            });

    },*/
    buildInputs: function () {
        $this = $(this);
        $.each(Calc.config.$inputs, function ( key, value ) {
        
            switch (value.type) {
                case 'text':
                case 'select':
                    value.class = 'form-control';
                break
                case 'button':
                case 'submit':
                    value.class = 'btn btn-default';
                break
            }
            switch (value.format) {
                case 'float' :
                    value.errorMsg = "Только числа с переменной точкой";
                break
                case 'integer':
                case undefined: 
                    value.errorMsg = value.id + " должно быть целым числом";
                break
            }
            if (value.format == 'float') {
            } 

            $formGroup = $('<div class = "form-group"></div>').appendTo($this);
            
            if (value.label == undefined) {
                value.label = '';
            }
            var label = $('<label></label>').appendTo($formGroup)
                .attr ('for'    ,   value.id)
                .attr ('class'  ,   "col-sm-2 control-label" )
                .html('<img src = "' + Calc.$latexBaseURL + encodeURIComponent(value.label) + '" >')
                ;
            
            if (value.type != 'select') {
                $('<input>').appendTo($formGroup)
                    .attr ('type'   ,   value.type)
                    .attr ('class'  ,   value.class)
                    .attr ('id'     ,   value.id)
                    .attr ('value'  ,   value.value)
                    .attr ('placeholder'  ,   value.placeholder)
                    .attr ('data-validation' , 'number')
                    .attr ('data-validation-allowing' , value.format)
                    .attr ('data-validation-decimal-separator' , '.')
                    .attr ('data-validation-error-msg', value.errorMsg)
                    .wrap('<div class = "col-sm-10" ></div>');
                    ;
            }else {
                var selector =  $('<select></select>').appendTo($formGroup);
                selector     
                    .attr ('class'  ,   value.class)
                    .attr ('id'     ,   value.id)
                    .each( function () {
                        switch (value.variable) {
                            case "X" :
                            case "x" :
                                var obj = value.table;
                            break
                            case "Y" :
                            case "y" :
                                var obj = value.table[Object.keys(value.table)[0]];
                            break
                        }
                        $.each(obj, function (k, v) {
                            $('<option>' + k + '</option>').appendTo(selector);
                        });
                    })
                    .wrap('<div class = "col-sm-10" ></div>');
            }
            }); 
    },

    buildResult: function ( container ) {
       
        var scale  = Calc.config.$dimScale;

        $(container).html('<h1><img src = "' 
        + Calc.$latexBaseURL 
        + 'M_{12}="> ' 
        + Calc.config.$calculateMethod({
            w: parseFloat($('#' + Calc.config.$inputs.w.id).val()),
            W: parseFloat($('#' + Calc.config.$inputs.W.id).val()),
            a: parseFloat($('#' + Calc.config.$inputs.a.id).val()) * scale,
            A: parseFloat($('#' + Calc.config.$inputs.A.id).val()) * scale,
            d: parseFloat($('#' + Calc.config.$inputs.d.id).val()) * scale,
            D: parseFloat($('#' + Calc.config.$inputs.D.id).val()) * scale,
            x4: parseFloat($('#' + Calc.config.$inputs.x4.id).val()) * scale,
            matrix: Calc.config.$matrices.F
        }))
        + " Гн"
        ;
    },

    findClosest: function (object, value, type) {
        var diff = [];
        if (type == "y" || type == "Y") {
            var object = object[Object.keys(object)[0]];
        }
        var values = Object.keys(object);
        $.each(values, function ( k, v ) {
            diff.push( Math.abs( v - value ));
        });
        var keyOfMin = getKeyByValue( diff, Math.min.apply(null, diff));
        return values[keyOfMin];

    }
    /*,
 
    buildUrl: function() {
        return Calc.config.urlBase + Calc.$currentItem.attr( "id" );
    },
 
    showItem: function() {
        var Calc.$currentItem = $( this );
        Calc.getContent( Calc.showContent );
    },
 
    getContent: function( callback ) {
        var url = Calc.buildUrl();
        Calc.$currentItem.data( "container" ).load( url, callback );
    },
 
    showContent: function() {
        Calc.$currentItem.data( "container" ).show();
        Calc.hideContent();
    },
 
    hideContent: function() {
        Calc.$currentItem.siblings().each(function() {
            $( this ).data( "container" ).hide();
        });
    }
 */
};

// // // // 
//
//
//
//

//Just one call for inizialization

//
//
//
//
// // // //


$( document ).ready( Calc.init ); // Default Call
 
//$( document ).ready( Calc.init({ $calculateMethod: wWProduct }) );

// // // // 
//
//
//
//
