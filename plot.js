	$(function() {

		var sin = [], cos = [], exp = [], M1 = [], M2 = [];
		for (var i = 0; i < 14; i += 0.1) {
			sin.push([i, Math.sin(i)]);
			cos.push([i, Math.cos(i)]);
			exp.push([i, Math.exp(i - 4)]);
			M1.push([i, Calc.config.$calculateMethod ({
				w: 5,
	            W: 5,
	            a: 0.005,
	            A: 0.005,
	            d: 0.005,
	            D: 0.005,
	            x4: 0.001 * i,
	            matrix: Calc.config.$matrices.F
			}) * 1000000000]);

			M2.push([i, Calc.config.$calculateMethod ({
				w: 5,
	            W: 5,
	            a: 0.005,
	            A: 0.005,
	            d: 0.005,
	            D: 0.001 * i,
	            x4: 0.001,
	           	matrix: Calc.config.$matrices.F
	        }) * 1000000000]);
		}
		plot = $.plot("#placeholder", [
			//{ data: sin, label: "sin(x) = -0.00"},
			//{ data: cos, label: "cos(x) = -0.00" },
			//{ data: exp, label: "exp(x - 4)  = -0.00" }
			{ data: M1, label: "M(x_4)  = -0.00" },
			{ data: M2, label: "M(D)  = -0.00" }
		], {
			series: {
				lines: {
					show: true
				}
			},
			crosshair: {
				mode: "x"
			},
			grid: {
				hoverable: true,
				autoHighlight: false
			},
			/*yaxis: {
				min: -1.2,
				max: 1.2
			}*/
		});

		var legends = $("#placeholder .legendLabel");

		legends.each(function () {
			// fix the widths so they don't jump around
			$(this).css('width', $(this).width());
		});

		var updateLegendTimeout = null;
		var latestPosition = null;

		function updateLegend() {

			updateLegendTimeout = null;

			var pos = latestPosition;

			var axes = plot.getAxes();
			if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max ||
				pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) {
				return;
			}

			var i, j, dataset = plot.getData();
			for (i = 0; i < dataset.length; ++i) {

				var series = dataset[i];

				// Find the nearest points, x-wise

				for (j = 0; j < series.data.length; ++j) {
					if (series.data[j][0] > pos.x) {
						break;
					}
				}

				// Now Interpolate

				var y,
					p1 = series.data[j - 1],
					p2 = series.data[j];

				if (p1 == null) {
					y = p2[1];
				} else if (p2 == null) {
					y = p1[1];
				} else {
					y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);
				}

				legends.eq(i).text(series.label.replace(/=.*/, "= " + y.toFixed(2)));
			}
		}

		$("#placeholder").bind("plothover",  function (event, pos, item) {
			latestPosition = pos;
			if (!updateLegendTimeout) {
				updateLegendTimeout = setTimeout(updateLegend, 50);
			}
		});

	});